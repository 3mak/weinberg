<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>
<?php $first = true; ?>

    <div class="r-main is-hovered">
        <?php if(get_header_image()): ?>
            <picture class="c-picture--default is-fullwidth" data-css="c-picture">
                <!--[if IE 9]>
                <audio><![endif]-->    <!--[if IE 9]></audio><![endif]-->
                <img src="<?php header_image(); ?>" alt=""/>
            </picture>
        <?php endif; ?>
        <div class="c-section--default" data-css="c-section">
            <div class="section__content">
                <div class="c-text--default is-center" data-css="c-text">
                    <h2 class="text__headline">Neuigkeiten & Veranstaltungen<br> vom Phöbener Wachtelberg</h2>
                    <p class="text__copy"></p>
                </div>
            </div>
        </div>
        <div class="row">
            <?php if (have_posts()): ?><?php while (have_posts()) : the_post(); ?><?php if ($first == true): ?><?php $first = false; ?>
                <div class="columns small-12">
                    <article class="c-article--teaser is-large" data-css="c-article">
                        <div class="article__figure">
                            <figure class="c-figure--default" data-css="c-figure">
                                <div class="figure__wrapper">
                                    <picture class="c-picture--default" data-css="c-picture">
                                        <!--[if IE 9]>
                                        <audio><![endif]-->                            <!--[if IE 9]></audio>
                                        <![endif]--><img src="<?php the_post_thumbnail_url('medium') ?>" alt=""/>
                                    </picture>
                                </div>
                            </figure>
                        </div>
                        <header class="article__header">
                            <h1 class="article__header-headline"><?php the_title() ?></h1>
                            <div class="article__header-meta">
                                <time class="article__header-metaitem is-clock" datetime="">18.04.2016</time>
                                <div class="article__header-metaitem is-folder">Wolfpack</div>
                            </div>
                        </header>
                        <div class="article__content">
                            <?php the_excerpt(); ?>
                        </div>
                        <a class="c-cta--default is-small" data-css="c-cta" title="weiterlesen ..."
                           href="<?php the_permalink() ?>"> <span class="cta__icon"></span> <span class="cta__content">weiterlesen ...</span>
                        </a>
                    </article>
                </div>
            <?php endif ?><?php endwhile; ?><?php endif ?>
            <div class="columns small-12 medium-8">
                <?php if (have_posts()): ?><?php while (have_posts()) : the_post(); ?><?php if ($first == true): ?>
                    <article class="c-article--teaser" data-css="c-article">
                        <div class="article__figure">
                            <figure class="c-figure--default" data-css="c-figure">
                                <div class="figure__wrapper">
                                    <picture class="c-picture--default" data-css="c-picture">
                                        <!--[if IE 9]>
                                        <audio><![endif]-->                            <!--[if IE 9]></audio>
                                        <![endif]--><img src="<?php the_post_thumbnail_url('thumbnail') ?>" alt=""/>
                                    </picture>
                                </div>
                            </figure>
                        </div>
                        <header class="article__header">
                            <h1 class="article__header-headline"><?php the_title() ?></h1>
                            <div class="article__header-meta">
                                <time class="article__header-metaitem is-clock" datetime="">18.04.2016</time>
                                <div class="article__header-metaitem is-folder">Wolfpack</div>
                            </div>
                        </header>
                        <div class="article__content">
                            <?php the_excerpt(); ?>
                        </div>
                        <a class="c-cta--default is-small" data-css="c-cta" title="weiterlesen ..."
                           href="<?php the_permalink() ?>"> <span class="cta__icon"></span> <span class="cta__content">weiterlesen ...</span>
                        </a>
                    </article>
                <?php else: ?>
                    <?php $first = true; ?>
                <?php endif; ?>

                <?php endwhile; ?>

                <?php endif ?>
                <div class="c-pagination--default" data-css="c-pagination">
                    <?php weinberg_pagination(); ?>
                </div>
            </div>
            <div class="columns small-12 medium-3 medium-offset-1">
                <aside class="c-sidebar--default" data-css="c-sidebar">
                    <?php dynamic_sidebar('sidebar-widgets') ?>
                    <div class="sidebar__item">
                        <div class="sidebar__content">
                            <div class="c-instagram-feed--sidebar" data-css="c-instagram-feed"
                                 data-js-module="instagram-feed" data-js-options='{}'>
                                <ul class="instagram-feed__images" data-js-atom="instagram-images"></ul>
                                <div class="instagram-feed__text"></div>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>
<?php
get_footer();
