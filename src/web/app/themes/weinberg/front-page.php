<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
    <div class="c-slider--default" data-css="c-slider" data-js-module="slider">
        <div class="slider__actions" data-js-atom="slider-actions"></div>
        <div class="slider__list-wrapper" data-js-atom="slider-wrapper">
            <div class="slider__list" data-js-atom="slider-ribbon">
                <?php if(get_field('images_1')): ?>
                <div class="slider__item" data-js-atom="slider-item">
                    <picture class="c-picture--default is-fullwidth" data-css="c-picture">
                        <!--[if IE 9]><audio><![endif]-->
                        <!--[if IE 9]></audio><![endif]-->
                        <img src="<?php the_field('images_1')?>" alt=""/>
                    </picture>
                </div>
                <?php endif; ?>
                <?php if(get_field('images_1')): ?>
                <div class="slider__item" data-js-atom="slider-item">
                    <picture class="c-picture--default is-fullwidth" data-css="c-picture">
                        <!--[if IE 9]><audio><![endif]-->
                        <!--[if IE 9]></audio><![endif]-->
                        <img src="<?php the_field('images_2')?>" alt=""/>
                    </picture>
                </div>
                <?php endif; ?>
                <?php if(get_field('images_1')): ?>
                <div class="slider__item" data-js-atom="slider-item">
                    <picture class="c-picture--default is-fullwidth" data-css="c-picture">
                        <!--[if IE 9]><audio><![endif]-->
                        <!--[if IE 9]></audio><![endif]-->
                        <img src="<?php the_field('images_3')?>" alt=""/>
                    </picture>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="slider__pagination" data-js-atom="slider-pagination">
            <ol class="slider__pagination-list" data-js-atom="slider-pagination-list"></ol>
        </div>
    </div>
    <div class="c-section--default" data-css="c-section">
        <div class="section__content">
            <div class="c-text--default is-center" data-css="c-text">

                <h2 class="text__headline"><?php the_title() ?></h2>
                <p class="text__copy"><?php the_content() ?></p>
                <div class="text__cta-wrapper">
                    <a class="c-cta--default is-large" data-css="c-cta" title="mehr erfahren" href="<?= get_permalink(20)?>">
                        <span class="cta__icon"></span>
                        <span class="cta__content">mehr erfahren</span>
                    </a></div>
            </div>
        </div>
    </div>
    <div class="c-section--default" data-css="c-section" style="background-image: url('<?= get_template_directory_uri()?>/img/home/startseite_bg_news.jpg')">
        <div class="section__content">
            <div class="row">
                <div class="columns small-12 medium-4">
                    <div class="c-text--headline is-center" data-css="c-text">
                        <h2 class="text__headline">Veranstaltungen</h2>
                        <p class="text__copy"></p>
                    </div>
                    <?php $query = weinberg_query('veranstaltungen'); ?>
                    <?php while( $query->have_posts() ): $query->the_post();  ?>
                        <div class="c-text--exerpt" data-css="c-text">
                            <h2 class="text__headline"><?php the_title() ?></h2>
                            <p class="text__copy"><?= wp_trim_words(get_the_excerpt(), 20)?></p>
                            <div class="text__cta-wrapper">
                                <a class="c-cta--default is-small" href="<?php the_permalink()?>" data-css="c-cta" title="weiter lesen ...">
                                    <span class="cta__icon"></span>
                                    <span class="cta__content">weiter lesen ...</span>
                                </a></div>
                        </div>
                    <?php endwhile;?>
                    <?php wp_reset_postdata() ?>
                </div>
                <div class="columns small-12 medium-4">
                    <div class="c-text--headline is-center" data-css="c-text">
                        <h2 class="text__headline">Neuigkeiten</h2>
                        <p class="text__copy"></p>
                    </div>
                    <?php $query = weinberg_query('neuigkeiten'); ?>
                    <?php while( $query->have_posts() ): $query->the_post();  ?>
                        <div class="c-text--exerpt" data-css="c-text">
                            <h2 class="text__headline"><?php the_title() ?></h2>
                            <p class="text__copy"><?= wp_trim_words(get_the_excerpt() , 20)?></p>
                            <div class="text__cta-wrapper">
                                <a class="c-cta--default is-small" href="<?php the_permalink(); ?>" data-css="c-cta" title="weiter lesen ...">
                                    <span class="cta__icon"></span>
                                    <span class="cta__content">weiter lesen ...</span>
                                </a></div>
                        </div>
                    <?php endwhile;?>
                    <?php wp_reset_postdata() ?>
                </div>
                <div class="columns small-12 medium-4">
                    <div class="c-text--headline is-center" data-css="c-text">
                        <h2 class="text__headline">shop</h2>
                        <p class="text__copy"></p>
                    </div>
                    <figure class="c-figure--shop" data-css="c-figure">
                        <div class="figure__wrapper">
                            <picture class="c-picture--default" data-css="c-picture">
                                <!--[if IE 9]>
                                <audio><![endif]-->                <!--[if IE 9]></audio><![endif]-->
                                <img src="<?php the_field('shop_image') ?>" alt="<?php the_field('shop_text') ?>"/>
                            </picture>
                        </div>
                        <figcaption class="figure__caption">
                            <div class="figure__caption-inner">
                                <p class="figure__caption-content"><?php the_field('shop_text') ?></p>
                            </div>
                        </figcaption>
                    </figure>
                    <a class="c-cta--default is-center is-large	" data-css="c-cta" title="Zum Shop" href="<?php the_field('shop_url') ?>">
                        <span class="cta__icon"></span>
                        <span class="cta__content">Zum Shop</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="c-section--default is-large-padding" data-css="c-section">
        <div class="section__content">
            <div class="row align-middle">
                <div class="columns small-12 medium-6">
                    <picture class="c-picture--default" data-css="c-picture">
                        <!--[if IE 9]>
                        <audio><![endif]-->    <!--[if IE 9]></audio><![endif]-->
                        <img src="<?= get_template_directory_uri()?>/img/home/startseite_weinpatenschaft.jpg" alt="Simple Picture"/>
                    </picture>
                </div>
                <div class="columns small-12 medium-6">
                    <div class="c-text--default is-center" data-css="c-text">
                        <h2 class="text__headline"><?php the_field('weinpatenschaft_title') ?></h2>
                        <p class="text__copy"><?php the_field('weinpatenschaft_text') ?></p>
                        <div class="text__cta-wrapper">
                            <a class="c-cta--default is-center is-large	" data-css="c-cta" title="Weinpate werden" href="<?php the_field('weinpatenschaft_link') ?>">
                                <span class="cta__icon"></span>
                                <span class="cta__content">Weinpate werden</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="c-section--default is-light-green" data-css="c-section">
        <div class="section__content">
            <div class="c-text--default is-center" data-css="c-text">
                <h2 class="text__headline"><?php the_field('contact_title') ?></h2>
                <p class="text__copy"><?php the_field('contact_text') ?></p>
            </div>
            <div class="b-address">
                <div class="address__item">
                    <div class="address__icon is-address"></div>
                    <div class="address__copy"><a target="_blank" href="<?php the_field('contact_adress_link') ?>"><?php the_field('contact_adress') ?></a></div>
                </div>
                <div class="address__item">
                    <div class="address__icon is-telephone"></div>
                    <div class="address__copy"><?php the_field('contact_telephone') ?></div>
                </div>
                <div class="address__item">
                    <div class="address__icon is-email"></div>
                    <div class="address__copy"><a href="mailto:<?php the_field('contact_email') ?>"><?php the_field('contact_email') ?></a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="c-section--default is-spring-rain" data-css="c-section">
        <div class="section__content">
            <div class="row align-center">
                <div class="columns medium-8">
                    <div class="b-contact-form">
                        <div class="c-form--default" data-css="c-form" data-js-module="form">
                            <form action="" class="form__form">
                                <div class="row">
                                    <div class="columns small-12 medium-6">
                                        <label for="name">Name</label> <input type="text" id="name">
                                    </div>
                                    <div class="columns small-12 medium-6">
                                        <label for="email">Email</label> <input type="text" id="email">
                                    </div>
                                    <div class="columns small-12 medium-12">
                                        <label for="email">Text</label>
                                        <textarea name="text" id="text" cols="30" rows="10"></textarea>
                                    </div>
                                    <div class="columns small-12 medium-12">
                                        <button class="c-cta--default is-dark-green is-center is-large" data-css="c-cta" title="Nachricht senden">
                                            <span class="cta__icon"></span>
                                            <span class="cta__content">Nachricht senden</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="b-maps" data-js-module="maps" data-js-options='{}'></div>
<?php endwhile;?>
<?php get_footer(); ?>