<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>

	<div class="r-main <?php if(has_post_thumbnail()):echo 'is-hovered'; endif ?>">
		<?php if ( have_posts() ): ?>
			<?php while ( have_posts() ) : the_post();  ?>
                <?php if(has_post_thumbnail()): ?>
				<picture class="c-picture--default is-fullwidth" data-css="c-picture">
					<!--[if IE 9]>
					<audio><![endif]-->    <!--[if IE 9]></audio><![endif]-->
					<img src="<?php the_post_thumbnail_url(); ?>" alt=""/>
				</picture>
                <?php endif; ?>
				<div class="c-section--default" data-css="c-section">
					<div class="section__content">
						<div class="c-text--default" data-css="c-text">
							<h2 class="text__headline"><?php the_title() ?></h2>
							<div class="text__copy"><?php the_content(); ?></div>
						</div>
					</div>
				</div>
                <?php if(get_field('form_tag')): ?>
                    <div class="c-section--default is-spring-rain is-large-padding" data-css="c-section">
                        <div class="section__content">
                            <div class="row align-center">
                                <div class="columns medium-7">
                                    <div class="c-text--default is-celtic" data-css="c-text">
                                        <h2 class="text__headline">Anmeldung</h2>
                                        <p class="text__copy"></p>
                                    </div>
                                    <div class="b-signup-form" data-js-module="signup-form" data-js-options='{}'>
                                        <div class="c-form--default" data-css="c-form" data-js-module="form">
                                            <?= do_shortcode(get_field('form_tag'))?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
			<?php endwhile;?>
		<?php endif; ?>
	</div>
<?php
get_footer();
