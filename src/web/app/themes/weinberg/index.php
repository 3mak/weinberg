<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>

	<div class="r-main is-hovered">
		<?php if ( have_posts() ): ?>
		<?php while ( have_posts() ) : the_post();  ?>
		<picture class="c-picture--default is-fullwidth" data-css="c-picture">
			<!--[if IE 9]>
			<audio><![endif]-->    <!--[if IE 9]></audio><![endif]-->
			<img src="<?php the_post_thumbnail_url(); ?>" alt=""/>
		</picture>
		<div class="c-section--default" data-css="c-section">
			<div class="section__content">
				<div class="c-text--default" data-css="c-text">
					<h2 class="text__headline"><?php the_title() ?></h2>
					<div class="text__copy"><?php the_content(); ?></div>
				</div>
			</div>
		</div>
		<?php endwhile;?>
		<?php endif; ?>
	</div>
<?php
get_footer();
