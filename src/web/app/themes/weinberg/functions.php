<?php
/**
 * _s functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package _s
 */

if (!function_exists('weinberg_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function weinberg_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on _s, use a find and replace
         * to change 'weinberg' to the name of your theme in all the template files.
         */
        load_theme_textdomain('weinberg', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        #add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        #add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'primary' => esc_html__('Primary', 'weinberg'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

    }
endif;
add_action('after_setup_theme', 'weinberg_setup');


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function weinberg_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'weinberg'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'weinberg'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'weinberg_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function weinberg_scripts()
{
    wp_enqueue_style('source-sans-pro', 'https://fonts.googleapis.com/css?family=Source+Sans+Pro', array(), '1.0.0');
    wp_enqueue_style('cinzel', 'https://fonts.googleapis.com/css?family=Cinzel+Decorative', array(), '1.0.0');
    wp_enqueue_style('css', get_template_directory_uri() . '/css/styles.css', array(), '1.0.0');
    wp_enqueue_script('google', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDZbSIDb1EjKfn5LTcmwzpWMPYgInSLBZk', array(), false   , true);
    wp_enqueue_script('libs', get_template_directory_uri() . '/js/vendor/libs.js', array(), '1.0.0', true);
    wp_enqueue_script('main', get_template_directory_uri() . '/js/main.js', array(), '1.0.0', true);
}

add_action('wp_enqueue_scripts', 'weinberg_scripts');


function register_my_menus() {
    register_nav_menus(
        array(
            'header-menu' => __( 'Header Menu' ),
            'footer-menu' => __( 'Footer Menu' )
        )
    );
}
add_action( 'init', 'register_my_menus' );

function weinberg_navigation() {
    return wp_nav_menu(array(
        'theme_location' => 'header-menu',
        'menu_class' => 'menu__list',
    ));
}

function weinberg_navigation_footer() {
    return wp_nav_menu(array(
        'theme_location' => 'footer-menu',
        'menu_class' => 'menu__list',
    ));
}


// Pagination.
if ( ! function_exists( 'weinberg_pagination' ) ) :
    function weinberg_pagination() {
        global $wp_query;

        $big = 999999999; // This needs to be an unlikely integer

        // For more options and info view the docs for paginate_links()
        // http://codex.wordpress.org/Function_Reference/paginate_links
        $paginate_links = paginate_links( array(
            'base' => str_replace( $big, '%#%', html_entity_decode( get_pagenum_link( $big ) ) ),
            'current' => max( 1, get_query_var( 'paged' ) ),
            'total' => $wp_query->max_num_pages,
            'mid_size' => 5,
            'prev_next' => true,
            'prev_text' => __( '&laquo;', 'foundationpress' ),
            'next_text' => __( '&raquo;', 'foundationpress' ),
            'type' => 'list',
        ) );

        $paginate_links = str_replace( "<ul class='page-numbers'>", "<ul class='pagination__list'>", $paginate_links );
        $paginate_links = str_replace( "<li>", "<li class='pagination__list-item'>", $paginate_links );
        $paginate_links = str_replace( "page-numbers", "pagination__list-element", $paginate_links );
        $paginate_links = str_replace( "<li class='pagination__list-item'><span class='pagination__list-element current'>", "<li class='pagination__list-item is-active'><span class='pagination__list-element'>", $paginate_links );

//	$paginate_links = str_replace( '<li><a class="page-numbers', "<li class='pagination__list-item'><a class='pagination__list-element'", $paginate_links );
//	$paginate_links = str_replace( "<li><span class='page-numbers current'>", "<li class='pagination__list-item is-active'><a class='pagination__list-element' href='#'>", $paginate_links );
//	$paginate_links = str_replace( '</span>', '</a>', $paginate_links );
//	$paginate_links = str_replace( "<li><a href='#'>&hellip;</a></li>", "<li><span class='dots'>&hellip;</span></li>", $paginate_links );
//	$paginate_links = preg_replace( '/\s*page-numbers/', '', $paginate_links );

        // Display the pagination if more than one page is found.
        if ( $paginate_links ) {
            echo '<div class="pagination-centered">';
            echo $paginate_links;
            echo '</div><!--// end .pagination -->';
        }
    }
endif;

/**
 * Add menu item class
 */
if (!function_exists('weinbergress_add_menuitemclass')) {
    function weinbergress_add_menuitemclass($classes, $item)
    {
        $new_classes = array('menu__item');

        if(in_array('active', $classes)) {
            array_push($new_classes, 'is-active');
        }

        if($item->menu_item_parent > 0) {
            array_push($new_classes, 'is-submenu');
        }

        return $new_classes;
    }

    add_filter('nav_menu_css_class', 'weinbergress_add_menuitemclass', 10 , 3 );
}

/**
 * Add menu item anchor attributes
 */
if (!function_exists('foundationpress_add_nav_menu_link_attributes')) {
    function foundationpress_add_nav_menu_link_attributes($atts, $item)
    {
        if($item->menu_item_parent > 0) {
            return array_merge( $atts, array('class' => 'menu__submenu-link', 'data-js-atom' => 'link') );
        }

        return array_merge( $atts, array('class' => 'menu__link ', 'data-js-atom' => 'link') );
    }

    add_filter('nav_menu_link_attributes', 'foundationpress_add_nav_menu_link_attributes', 10 , 3 );
}

/**
 * @param $category
 * @return WP_Query
 */
function weinberg_query($category) {
    // WP_Query arguments
    $args = array(
        'post_type'              => array( 'post' ),
        'category_name'          => $category,
        'posts_per_page'         => 3
    );

    // The Query
    $query = new WP_Query( $args );

    return $query;
}

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
#require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
#require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
#require get_template_directory() . '/inc/jetpack.php';
require get_template_directory() . '/inc/widget-areas.php';
