<?php
/**
 * Template Name: Contact
 * @package _s
 */

get_header(); ?>

	<div class="r-main isnt-margin-bottom <?php if(has_post_thumbnail()):echo 'is-hovered'; endif ?>">
		<?php if ( have_posts() ): ?>
			<?php while ( have_posts() ) : the_post();  ?>
				<picture class="c-picture--default is-fullwidth" data-css="c-picture">
					<!--[if IE 9]>
					<audio><![endif]-->    <!--[if IE 9]></audio><![endif]-->
					<img src="<?php the_post_thumbnail_url(); ?>" alt=""/>
				</picture>
				<div class="c-section--default is-light-green" data-css="c-section">
					<div class="section__content">
						<div class="c-text--default" data-css="c-text">
							<h2 class="text__headline"><?php the_title() ?></h2>
							<div class="text__copy"><?php the_content(); ?></div>
						</div>
					</div>
				</div>
                <div class="c-section--default is-spring-rain" data-css="c-section">
                    <div class="section__content">
                        <div class="row align-center">
                            <div class="columns medium-8">
                                <div class="b-contact-form">
                                    <div class="c-form--default" data-css="c-form" data-js-module="form">
                                        <?php echo do_shortcode('[contact-form-7 id="6" title="Contact form 1" html_class="form__form"]')?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			<?php endwhile;?>
		<?php endif; ?>
	</div>
<?php
get_footer();
