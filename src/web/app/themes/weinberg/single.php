<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package _s
 */

get_header(); ?>

    <div class="r-main ">


        <div class="row">


            <div class="columns large-8">
                <?php while ( have_posts() ) : the_post();?>
                <article class="c-article--news" data-css="c-article">

                    <div class="article__figure">

                        <figure
                                class="c-figure--default"
                                data-css="c-figure">

                            <div class="figure__wrapper">

                                <picture class="c-picture--default" data-css="c-picture">

                                    <!--[if IE 9]>
                                    <audio><![endif]-->


                                    <!--[if IE 9]></audio><![endif]-->

                                    <img src="<?= get_the_post_thumbnail_url()?>" alt=""/>
                                </picture>
                            </div>


                        </figure>
                    </div>

                    <header class="article__header">
                        <h1 class="article__header-headline"><?php the_title()?></h1>


                        <div class="article__header-meta">
                            <time class="article__header-metaitem is-clock" datetime="<?php get_post_time(); ?>"><?= get_post_time('d.m.Y')?></time>
                            <div class="article__header-metaitem is-folder">
                                <?php foreach (get_the_category() as $cat): ?>
                                    <?= $cat->name?>
                                <?php endforeach;?>
                            </div>
                        </div>

                    </header>

                    <div class="article__content">
                        <div class="c-rte--default" data-css="c-rte">
                            <?php the_content()?>
                        </div>
                    </div>


                </article>

                <div class="b-article-gallery"
                     data-js-module="article-gallery"
                     data-js-options='{
                         	"": ""
                         }'>
                    <div class="article-gallery__item">

                        <figure
                                class="c-figure--default"
                                data-css="c-figure">

                            <div class="figure__wrapper">

                            </div>


                        </figure>
                    </div>
                    <div class="article-gallery__item">

                        <figure
                                class="c-figure--default"
                                data-css="c-figure">

                            <div class="figure__wrapper">

                            </div>


                        </figure>
                    </div>
                    <div class="article-gallery__item">

                        <figure
                                class="c-figure--default"
                                data-css="c-figure">

                            <div class="figure__wrapper">

                            </div>


                        </figure>
                    </div>
                    <div class="article-gallery__item">

                        <figure
                                class="c-figure--default"
                                data-css="c-figure">

                            <div class="figure__wrapper">

                            </div>


                        </figure>
                    </div>
                    <div class="article-gallery__item">

                        <figure
                                class="c-figure--default"
                                data-css="c-figure">

                            <div class="figure__wrapper">

                            </div>


                        </figure>
                    </div>
                    <div class="article-gallery__item">

                        <figure
                                class="c-figure--default"
                                data-css="c-figure">

                            <div class="figure__wrapper">

                            </div>


                        </figure>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>


            <div class="columns large-3 large-offset-1">

                <aside class="c-sidebar--default" data-css="c-sidebar">
                    <?php dynamic_sidebar('sidebar-widgets') ?>
                    <div class="sidebar__item">
                        <div class="sidebar__content">
                            <div class="c-instagram-feed--sidebar" data-css="c-instagram-feed"
                                 data-js-module="instagram-feed" data-js-options='{}'>
                                <ul class="instagram-feed__images" data-js-atom="instagram-images"></ul>
                                <div class="instagram-feed__text"></div>
                            </div>
                        </div>
                    </div>
                </aside>

            </div>


        </div>
    </div>
<?php get_footer(); ?>
