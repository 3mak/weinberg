<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
			<div class="r-footer">
				<div class="c-menu--footer" data-css="c-menu" data-js-module="menu">
                    <?php weinberg_navigation_footer(); ?>
				</div>
				<div class="b-footer-text">
					<a href="https://www.instagram.com/Phoebenerwachtelberg/" target="_blank" class="footer-text__link">Folgen Sie uns auf Instagram</a>
					<div class="footer-text__text">© 2016 - Phoebener-Wachtelberg.de | <a href="https://www.erstellbar.de" target="_blank" class="footer-text__text" style="text-decoration: none;" title="Webdesign und Entwicklung aus einer Hand">Webdesign by Erstellbar.de</a>
						
						
						
				</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php wp_footer(); ?>

</body>
</html>
