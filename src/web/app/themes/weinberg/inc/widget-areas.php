<?php
/**
 * Register widget areas
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

if ( ! function_exists( 'weinberg_sidebar_widgets' ) ) :
function weinberg_sidebar_widgets() {
	register_sidebar(array(
	  'id' => 'sidebar-widgets',
	  'name' => __( 'Sidebar widgets', 'weinberg' ),
	  'description' => __( 'Drag widgets to this sidebar container.', 'weinberg' ),
	  'before_widget' => '<article id="%1$s" class="sidebar__item %2$s">',
	  'after_widget' => '</article>',
	  'before_title' => '<h3 class="sidebar__headline">',
	  'after_title' => '</h3>',
	));
}

add_action( 'widgets_init', 'weinberg_sidebar_widgets' );
endif;
