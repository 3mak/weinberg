<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="r-header">
	<div class="b-navigation" data-js-module="navigation" data-js-options='{
         	
         }'>
		<div class="navigation__inner">
			<div class="navigation__icon-wrapper" data-js-atom="hamburger-icon">
				<div class="navigation__icon"></div>
			</div>
			<div class="navigation__logo"><a href="/"></a></div>
			<div class="navigation__menu">
				<div class="c-menu--default" data-css="c-menu" data-js-module="menu">
					<?php echo weinberg_navigation(); ?>
				</div>
			</div>
			<div class="navigation__cta">
				<a class="c-cta--default" data-css="c-cta" title="Shop" target="_blank" href="https://supr.com/phoebenerwachtelberg/ ">
					<span class="cta__icon"></span>
					<span class="cta__content">Shop</span>
				</a></div>
		</div>
	</div>
</div>
<div class="c-offcanvas--default" data-css="c-offcanvas" data-js-module="offcanvas">
	<div class="offcanvas__sidebar">
		<div class="c-menu--offcanvas" data-css="c-menu" data-js-module="menu">
            <?php echo weinberg_navigation(); ?>
		</div>
	</div>
	<div class="offcanvas__content">
		<div class="r-main is-hovered">
