module.exports = function(Handlebars) {

window["App"] = window["App"] || {};
window["App"]["Templates"] = window["App"]["Templates"] || {};

window["App"]["Templates"]["INSTAGRAMFEEDITEM"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<li class=\"instagram-feed__item\">\n	<figure class=\"c-figure--instagram\" data-css=\"c-figure\">\n		<a href=\"{{link}}\" target=\"_blank\">					 			\n			<div class=\"figure__image-wrapper\">\n				<img class=\"figure__image\" src=\"{{image}}\">  \n			</div>\n		</a>\n 	</figure>\n</li>\n";
},"useData":true});

return window["App"]["Templates"];

};