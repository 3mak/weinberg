/**
 * Const for events (pub/sub)
 *
 * @author: Sebastian Fitzner
 */

/**
 * Events Global
 */

const EVENTS = {
	DOMchanged: 'DOMchanged',
	DOMredirect: 'dom:redirect',
	mediachange: 'mediachange',
	resize: 'resize',
	scroll: 'scroll',
	mouseover: "mouseover",
	mouseout: "mouseout",
	click: "click",
	animationEnd: "animationend webkitAnimationEnd oanimationEnd MSAnimationEnd",
	transitionEnd: "transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd"
};


/**
 * Events for Releases
 */
EVENTS.releases = {
	eventName: 'releases:eventName'
};

/**
 * Events for Offcanvas
 */
EVENTS.offcanvas = {
	toggle: 'offcanvas:toggle'
};

/**
 * Events for Datelist
 */
EVENTS.datelist = {
	load: 'datelist:load'
};


// @INSERTPOINT :: @ref: js-events

export default EVENTS;
