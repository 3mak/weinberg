/**
 * Menu Module
 *
 * @module Menu
 * @version v0.0.1
 *
 * @author Sebastian Fitzner
 * @author Andy Gutsche
 */

import Helpers from '../../utils/helpers';
import App from '../../app';
import AppModule from '../_global/module';
const $ = App.$;

class Menu extends AppModule {
	/**
	 * Constructor for our class
	 *
	 * @see module.js
	 *
	 * @param {obj} obj - Object which is passed to our class
	 * @param {obj.el} obj - element which will be saved in this.el
	 * @param {obj.options} obj - options which will be passed in as JSON object
	 */
	constructor(obj) {
		let options = {
			activeClass: 'is-active',
			linkSel: '[data-js-atom="link"]',
			subMenuSel: '[data-js-atom="submenu"]',

		};

		super(obj, options);
		App.registerModule && App.registerModule(Menu.info, this.el);
	}

	/**
	 * Get module information
	 */
	static get info() {
		return {
			name: 'Menu',
			version: '0.0.1',
			vc: true,
			mod: false // set to true if source was modified in project
		};
	}


	/**
	 * Initialize the view and merge options
	 *
	 */
	initialize() {
		console.log('Menu');
		this.$menuLink = $(this.options.linkSel, this.$el);
		this.$subMenus  = $(this.options.subMenuSel, this.$el);

		super.initialize();
	}

	/**
	 * Bind events
	 *
	 * Listen to open and close events
	 */
	bindEvents() {
		this.$menuLink.on(App.EVENTS.click, this.toggleSubmenu.bind(this));
		//this.$menuLink.on(App.EVENTS.mouseout, this.closeSubmenu.bind(this));
	}

	toggleSubmenu(e) {
		let $target = $(e.currentTarget);
		let $submenu = $target.siblings(this.options.subMenuSel);
		let submenuActive = $submenu.hasClass(this.options.activeClass);

		if ($submenu.length > 0) {
			e.preventDefault();
			this.$subMenus.not($submenu).removeClass(this.options.activeClass);
			$submenu.toggleClass(this.options.activeClass);
		}


	}
}

export default Menu;
