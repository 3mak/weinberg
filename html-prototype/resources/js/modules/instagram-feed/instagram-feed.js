/**
 * Description of InstagramFeed.
 *
 * @module InstagramFeed
 * @version v0.0.0
 *
 * @author your_name
 */

import Helpers from '../../utils/helpers';
import App from '../../app';
import AppModule from '../_global/module';
const $ = App.$;

var Instafeed = require("instafeed.js");
let Handlebars = require('handlebars/runtime')['default'];
let Template = require('../../templates/templates')(Handlebars);

class InstagramFeed extends AppModule {
	/**
	 * Constructor for our class
	 *
	 * @see module.js
	 *
	 * @param {Object} obj - Object which is passed to our class
	 * @param {Object} obj.el - element which will be saved in this.el
	 * @param {Object} obj.options - options which will be passed in as JSON object
	 */
	constructor(obj) {
		let options = {
			target: '[data-js-atom="instagram-images"]',
			instagram: {
				get: 'user',
				userId: '3918262617',
				clientId: 'b7da86aaca474f09a4b1230ab16e2f4d',
				accessToken: '3918262617.1677ed0.c14d458402134341a2716ebeccf9c1ac',
				limit: 4,
			}
		};

		super(obj, options);
		App.registerModule && App.registerModule(InstagramFeed.info, this.el);
	}

	/**
	 * Get module information
	 */
	static get info() {
		return {
			name: 'InstagramFeed',
			version: '0.0.1'
		};
	}

	/**
	 * Initialize the view and merge options
	 *
	 */
	initialize() {

		let options = {
			template: Template.INSTAGRAMFEEDITEM(),
			target: this.el.querySelector(this.options.target)
		};
		options = $.extend(options, this.options.instagram);
		this.feed = new Instafeed(options);

		super.initialize();
	}

	/**
	 * Bind events
	 *
	 * Listen to open and close events
	 */
	bindEvents() {
		// Global events

		// Local events
	}

	/**
	 * Render class
	 */
	render() {
		this.feed.run();
	}
}

export default InstagramFeed;
