/**
 * Description of Maps.
 *
 * @module Maps
 * @version v0.0.0
 *
 * @author your_name
 */

import Helpers from '../../utils/helpers';
import App from '../../app';
import AppModule from '../_global/module';
const $ = App.$;

class Maps extends AppModule {
	/**
	 * Constructor for our class
	 *
	 * @see module.js
	 *
	 * @param {Object} obj - Object which is passed to our class
	 * @param {Object} obj.el - element which will be saved in this.el
	 * @param {Object} obj.options - options which will be passed in as JSON object
	 */
	constructor(obj) {
		let options = {};

		super(obj, options);
		App.registerModule && App.registerModule(Maps.info, this.el);
	}

	/**
	 * Get module information
	 */
	static get info() {
		return {
			name: 'Maps',
			version: '0.0.0'
		};
	}

	/**
	 * Initialize the view and merge options
	 *
	 */
	initialize() {
		console.log('init Maps');
		super.initialize();
	}

	/**
	 * Bind events
	 *
	 * Listen to open and close events
	 */
	bindEvents() {
		// Global events

		// Local events
	}

	/**
	 * Render class
	 */
	render() {
		new google.maps.Map(document.querySelector('[data-js-module="maps"]'), {
			center: {lat: 52.424790, lng: 12.884480},
			zoom: 16,
			scrollwheel: false
		});
	}
}

export default Maps;