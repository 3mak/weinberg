/**
 * Description of Navigation.
 *
 * @module Navigation
 * @version v0.0.0
 *
 * @author your_name
 */

import Helpers from '../../utils/helpers';
import App from '../../app';
import AppModule from '../_global/module';
const $ = App.$;

class Navigation extends AppModule {
	/**
	 * Constructor for our class
	 *
	 * @see module.js
	 *
	 * @param {Object} obj - Object which is passed to our class
	 * @param {Object} obj.el - element which will be saved in this.el
	 * @param {Object} obj.options - options which will be passed in as JSON object
	 */
	constructor(obj) {
		let options = {
			'fixedClass': 'is-fixed',
			'hamburgerSel': '[data-js-atom="hamburger-icon"]',
			'menuSel': '[data-js-module="menu"]'
		};

		super(obj, options);
		App.registerModule && App.registerModule(Navigation.info, this.el);
	}

	/**
	 * Get module information
	 */
	static get info() {
		return {
			name: 'Navigation',
			version: '0.0.0'
		};
	}

	/**
	 * Initialize the view and merge options
	 *
	 */
	initialize() {
		console.log('init Navigation');

		this.navigationPosition = 60;
		this.$hamburger = $(this.options.hamburgerSel, this.$el);
		this.$menu = $(this.options.menuSel, this.$el);
		super.initialize();
	}

	/**
	 * Bind events
	 *
	 * Listen to open and close events
	 */
	bindEvents() {
		let fixNavigationFn = this.fixNavigation.bind(this);
		let triggerOffCanvasFn = this.triggerOffCanvas.bind(this);
		// Global events

		// Local events
		App.Vent.on(App.EVENTS.scroll, fixNavigationFn);
		this.$hamburger.on(App.EVENTS.click, triggerOffCanvasFn);
	}

	fixNavigation() {
		let scrollTop = $(document).scrollTop();

		if(scrollTop >= this.navigationPosition) {
			this.$el.addClass(this.options.fixedClass);
			this.$menu.addClass(this.options.fixedClass);
		}

		if(scrollTop < this.navigationPosition) {
			this.$el.removeClass(this.options.fixedClass);
			this.$menu.removeClass(this.options.fixedClass);
		}
	}

	triggerOffCanvas() {
		this.$hamburger.toggleClass('is-active');
		App.Vent.trigger(App.EVENTS.offcanvas.toggle);
	}

	/**
	 * Render class
	 */
	render() {

	}
}

export default Navigation;