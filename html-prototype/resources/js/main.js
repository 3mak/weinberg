// Main Requirements
import App from './app';
import Helpers from './utils/helpers';

// ES6 Modules
import CTA from './modules/cta/cta';
import Slider from './modules/slider/slider';
import Navigation from './modules/navigation/navigation';
import InstagramFeed from './modules/instagram-feed/instagram-feed';
import Offcanvas from './modules/offcanvas/offcanvas';


import Form from './modules/form/form';


import Maps from './modules/maps/maps';


import SignupForm from './modules/signup-form/signup-form';

// @INSERTPOINT :: @ref: js-import

// Vars
const $ = App.$;

'use strict';

// Main Functionality
class Core {
    constructor() {
        this.initialize();
    }

    /**
     * Initialize our core functionality
     * This function will only be executed once.
     */
    initialize() {
        console.log('App initialized with version: ', App.version);

        /**
         * Detect Touch
         */
        if (!App.support.touch) {
            $('html').addClass('no-touch');
        } else {

            $('html').addClass('touch');
        }

        // Redirect
        App.Vent.on(App.EVENTS.DOMredirect, (obj) => {
            if (!obj && !obj.url) throw new Error('Object is not defined. Please provide an url in your object!');

            // Redirect to page
            window.location.href = String(obj.url);
        });

        // @INSERTPOINT :: @ref: js-init-once-v3

    }

    preRender() {
        Helpers.saveDOM();
    }

    render(context) {

        /**
         * Init Call-To-Action
         */
        Helpers.loadModule({
            domName: 'cta',
            module: CTA,
            context: context
        });


        /**
         * Init Slider
         */
        Helpers.loadModule({
            domName: 'slider',
            module: Slider,
            context: context
        });

        /**
         * Init Navigation
         */
        Helpers.loadModule({
            domName: 'navigation',
            module: Navigation,
            context: context
        });


        /**
         * Init Form
         */
        Helpers.loadModule({
            domName: 'form',
            module: Form,
            context: context
        });


        /**
         * Init Maps
         */
        Helpers.loadModule({
            domName: 'maps',
            module: Maps,
            context: context
        });

        /**
         * Init InstagramFeed
         */
        Helpers.loadModule({
            domName: 'instagram-feed',
            module: InstagramFeed,
            context: context
        });

        /**
         * Init Offcanvas
         */
        Helpers.loadModule({
            domName: 'offcanvas',
            module: Offcanvas,
            context: context
        });

        /**
         * Init SignupForm
         */
        Helpers.loadModule({
            domName: 'signup-form',
            module: SignupForm,
            context: context
        });

        // @INSERTPOINT :: @ref: js-init-v3

    }
}

document.addEventListener("DOMContentLoaded", function () {
    let core = new Core();

    /**
     * Render modules
     */
    core.preRender();
    core.render(document);

    /**
     * Initialize modules which are loaded after initial load
     * via custom event 'DOMchanged'
     */
    App.Vent.on(App.EVENTS.DOMchanged, (context) => {
        console.log('Dom has changed. Initialising new modules in: ', context);
        core.preRender();
        core.render(context);
    });
});
