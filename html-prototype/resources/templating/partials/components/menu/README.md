# menu

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: menu, @tag: component-partial }}
{{#with menu-bp}}
	{{> c-menu}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "components/_c-menu";
// @INSERT :: END
```
