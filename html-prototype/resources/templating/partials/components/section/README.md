# section

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: section, @tag: component-partial }}
{{#with section-bp}}
	{{#wrapWith "c-section"}}
		Wrapped with markup from section.
	{{/wrapWith}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "components/_c-section";
// @INSERT :: END
```
