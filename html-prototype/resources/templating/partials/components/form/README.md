# form

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: form, @tag: component-partial }}
{{#with form-bp}}
	{{#wrapWith "c-form"}}
		Wrapped with markup from form.
	{{/wrapWith}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "components/_c-form";
// @INSERT :: END
```

### Include: JavaScript

#### Import
``` js
// @INSERT :: START @tag: js-import //
import Form from './modules/form/form';
// @INSERT :: END
```

#### Initializing in Veams V2
``` js
// @INSERT :: START @tag: js-init-v2 //
/**
 * Init Form
 */
Helpers.loadModule({
	el: '[data-js-module="form"]',
	module: Form,
	context: context
});
// @INSERT :: END
```

#### Initializing in Veams V3
``` js
// @INSERT :: START @tag: js-init-v3 //
/**
 * Init Form
 */
Helpers.loadModule({
	domName: 'form',
	module: Form,
	context: context
});
// @INSERT :: END
```

#### Custom Events
``` js
// @INSERT :: START @tag: js-events //
/**
 * Events for Form
 */
EVENTS.form = {
	eventName: 'form:eventName'
};
// @INSERT :: END
```
