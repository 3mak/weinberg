# maps

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: maps, @tag: block-partial }}
{{#with maps-bp}}
	{{> b-maps}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "/_b-maps";
// @INSERT :: END
```

### Include: JavaScript

#### Import
``` js
// @INSERT :: START @tag: js-import //
import Maps from './modules/maps/maps';
// @INSERT :: END
```

#### Initializing in Veams V2
``` js
// @INSERT :: START @tag: js-init-v2 //
/**
 * Init Maps
 */
Helpers.loadModule({
	el: '[data-js-module="maps"]',
	module: Maps,
	context: context
});
// @INSERT :: END
```

#### Initializing in Veams V3
``` js
// @INSERT :: START @tag: js-init-v3 //
/**
 * Init Maps
 */
Helpers.loadModule({
	domName: 'maps',
	module: Maps,
	context: context
});
// @INSERT :: END
```

#### Custom Events
``` js
// @INSERT :: START @tag: js-events //
/**
 * Events for Maps
 */
EVENTS.maps = {
	eventName: 'maps:eventName'
};
// @INSERT :: END
```
