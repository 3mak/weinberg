# contact-form

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: contact-form, @tag: block-partial }}
{{#with contact-form-bp}}
	{{> b-contact-form}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "/_b-contact-form";
// @INSERT :: END
```
