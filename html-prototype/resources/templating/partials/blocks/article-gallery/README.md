# article-gallery

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: article-gallery, @tag: block-partial }}
{{#with article-gallery-bp}}
	{{> b-article-gallery}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "/_b-article-gallery";
// @INSERT :: END
```

### Include: JavaScript

#### Import
``` js
// @INSERT :: START @tag: js-import //
import ArticleGallery from './modules/article-gallery/article-gallery';
// @INSERT :: END
```

#### Initializing in Veams V2
``` js
// @INSERT :: START @tag: js-init-v2 //
/**
 * Init ArticleGallery
 */
Helpers.loadModule({
	el: '[data-js-module="article-gallery"]',
	module: ArticleGallery,
	context: context
});
// @INSERT :: END
```

#### Initializing in Veams V3
``` js
// @INSERT :: START @tag: js-init-v3 //
/**
 * Init ArticleGallery
 */
Helpers.loadModule({
	domName: 'article-gallery',
	module: ArticleGallery,
	context: context
});
// @INSERT :: END
```

#### Custom Events
``` js
// @INSERT :: START @tag: js-events //
/**
 * Events for ArticleGallery
 */
EVENTS.articleGallery = {
	eventName: 'articleGallery:eventName'
};
// @INSERT :: END
```
