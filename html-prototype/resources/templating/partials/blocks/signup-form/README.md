# signup-form

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: signup-form, @tag: block-partial }}
{{#with signup-form-bp}}
	{{> b-signup-form}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "/_b-signup-form";
// @INSERT :: END
```

### Include: JavaScript

#### Import
``` js
// @INSERT :: START @tag: js-import //
import SignupForm from './modules/signup-form/signup-form';
// @INSERT :: END
```

#### Initializing in Veams V2
``` js
// @INSERT :: START @tag: js-init-v2 //
/**
 * Init SignupForm
 */
Helpers.loadModule({
	el: '[data-js-module="signup-form"]',
	module: SignupForm,
	context: context
});
// @INSERT :: END
```

#### Initializing in Veams V3
``` js
// @INSERT :: START @tag: js-init-v3 //
/**
 * Init SignupForm
 */
Helpers.loadModule({
	domName: 'signup-form',
	module: SignupForm,
	context: context
});
// @INSERT :: END
```

#### Custom Events
``` js
// @INSERT :: START @tag: js-events //
/**
 * Events for SignupForm
 */
EVENTS.signupForm = {
	eventName: 'signupForm:eventName'
};
// @INSERT :: END
```
