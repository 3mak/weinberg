# footer-navigation

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: footer-navigation, @tag: block-partial }}
{{#with footer-navigation-bp}}
	{{> b-footer-navigation}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "/_b-footer-navigation";
// @INSERT :: END
```
