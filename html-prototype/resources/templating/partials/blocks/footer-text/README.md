# footer-text

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: footer-text, @tag: block-partial }}
{{#with footer-text-bp}}
	{{> b-footer-text}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "/_b-footer-text";
// @INSERT :: END
```
